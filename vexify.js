Teams = new Mongo.Collection("teams")
//# movin arounnd the user with these
Router.route('/', {
  template: 'login'
});
Router.route('/home', {
  template: 'homePage'
});
Router.route('/register', {
  template: 'register'
});
Router.route('/teamListPage',{
  template: 'teamListPage'
})
Router.route('/settings',{
  template: 'settingsPage'
})
Router.route('/adminDash', {
  template: 'adminPage'
})
Router.route('/team/:_id',{
  template: 'teamViewPage',
  data:function(){
    var team = Teams.findOne({_id: this.params._id});
    return team;
  }
})
Router.route('/teamsaves',{
  template: 'savedTeamView'
});
Router.route('/teamStats/:_id',{
  template: 'viewTeamStats',
  data: function(){
    var team = Teams.findOne({_id: this.params._id});
    return team;
  }
})

var requireAdmin = function(){
  team = Teams.findOne({userID: Meteor.userId()});
  if(! Meteor.user()){
    this.render('login');
  }else{
    if(team.isAdmin){
      this.next();
    }else{
      this.render('homePage');
    }
  }
}

var requireLogin = function() {
  if (! Meteor.user()) {
   this.render('login');
 } else {
   this.next();
 }
}

var checkForBadWords = function(){


}

//  notifications: [{type: "info", asap: true, Title: "Welcome!", desc: "Welcome to vexify we are cool!" }]
var checkForNotifications = function(){
  currentTeam = Teams.findOne({userID: Meteor.userId()});
  if(!Meteor.user()){
    this.render('login');
  }else{
    notifications = currentTeam.notifications;
    notifications.forEach(function(noti, index, array){

      if(noti.asap){
        swal(noti.Title, noti.desc, noti.type);
        Meteor.call('removeNotification', currentTeam._id, {id: noti.id})
      }else{
      }

    });
  }
  this.next();
}

// Before any routing run the requireLogin function.
// Except in the case of "landingpage".
// Note that you can add more pages in the exceptions if you want. (e.g. About, Faq, contact...)

//Check if there are notifications
Router.onBeforeAction(checkForNotifications, {only: ['home']});
Router.onBeforeAction(requireLogin, {except: ['login', 'register']});
//Make sure proper user privs are present
Router.onBeforeAction(requireAdmin, {only: ['adminDash']});
Router.onBeforeAction(requireLogin, {except: ['login', 'register']});



//Publish data so the user can grab it
if(Meteor.isServer){
  Meteor.publish("Teams", function(){
    return Teams.find();
  });

  Meteor.methods({
    addTeam: function(userId, vars){
      if(Meteor.userID){
        throw new Meteor.error("Attempted hax? emailinggg admin");
      }else{
        //Meteor.call('addTeam', Meteor.userId(), {captainName: captainName, teamName: teamName, description: "", overall: 0, ratings: [], launcherSpeedRating: 0, intakeSpeedRating: 0, liftRating: 0, launcherAccuracy: 0})
        Teams.insert({userID: userId, captainName: vars.captainName, teamName: vars.teamName, description: "", overall: 0, isBlocked: false , adsDisabled: true, isAdmin: false, isChosen: false , ratings: [], savedTeams: [], notifications: [{id: "1", type: "info", asap: true, Title: "Welcome!", desc: "Welcome to vexify. We are currently still in beta so expect some bugs. If you have any questions, comments or recommendations please email support@llamachair.com" }]});
        console.log("Team created!" + Teams.findOne({userID: userId})._id);
      }
    },
    updateSettings: function(userID, vars){
      Team = Teams.findOne({userID: userID});
      if(vars.teamName == ""){
        vars.teamName = Team.teamName;

      }

      Teams.update({_id: Team._id}, {$set: {captainName: vars.capt, teamName: vars.teamName, description: vars.description}});
    },
    updateTeamRating: function(vars){
      Teams.update({_id: vars.teamId}, {$set: {ratings: vars.teamRating}});
    },
    updateTeamOverall: function(teamId, vars){
      var team = Teams.findOne({_id: teamId});
      var numberOfRatings = 0;
      var overall = 0;

      team.ratings.forEach(function(rating, index, array){
        var score = 0;
        numberOfRatings += 1;
        rating.launcherSpeedRating = parseInt(rating.launcherSpeedRating);
        rating.intakeSpeedRating = parseInt(rating.intakeSpeedRating);
        rating.liftRating = parseInt(rating.liftRating);
        rating.launcherAccuracy = parseInt(rating.launcherAccuracy);

        score = ( (rating.launcherSpeedRating * .8) * 10 + ( rating.intakeSpeedRating * .5 ) * 10 + ( rating.liftRating * .2 ) * 10 + rating.launcherAccuracy * 10) / 4;
        //TODO Test the math here
        score = Math.round(score);
        //console.log("Team socre: " + score);

        overall += score;

      });

      overall = overall  / numberOfRatings;

      if(numberOfRatings === 0){
        overall = 0;
      }

      Teams.update({_id: teamId}, {$set: {overall: overall}});
    },
    updateSavedTeams: function(userID, vars){
      Teams.update({userID: userID}, {$set: {savedTeams: vars.teamList}});
    },
    saveTeam: function(userID, vars){
      teamList = Teams.findOne({userID: userID}).savedTeams;

      teamList.push(vars.team);

      Teams.update({userID: userID}, {$set: {savedTeams: teamList}});
    },
    changeIsTeamTaken: function(teamID, vars){
      Teams.update({_id: teamID}, {$set: {isChosen: vars.isTaken}});
    },
    //Meteor.call('changeAdDisable', teamID, {isDisabled: value})
    changeAdDisable: function(teamID, vars){
      Teams.update({_id: teamID}, {$set: {adsDisabled: vars.isDisabled}});
    },
    changeTeamBan: function(teamID, vars){
      Teams.update({_id: teamID}, {$set: {isBlocked: vars.isBlocked}});
    },
    //var adsDis = Meteor.call('getTeamWithUserId', Meteor.userId(), {});
    getTeamWithUserId: function(userID, vars){
      var team = Teams.findOne({userID: userID});
      return team;
    },
    removeNotification: function(teamID, vars){
      var team = Teams.findOne({_id: teamID});
      newArr = team.notifications;
      newArr.forEach(function(note, index, array){
        if(note.id === vars.id){
          newArr.splice(index, 1);
        }
      });
      Teams.update({_id: teamID}, {$set: {notifications: newArr}});
    },
    addNotification: function(teamID, noti){
      var team = Teams.findOne({_id: teamID});
      notifications = team.notifications;
      noti.id = Math.floor((Math.random() * 500) + 1);
      notifications.push(noti);
      Teams.update({_id: teamID}, {$set: {notifications: noti}});
    }
  })
}

if (Meteor.isClient) {

  var currentViewingTeam;

  // This code only runs on the client
  Meteor.subscribe("Teams");  //Allow the client to access the teams database
  Template.settingsPage.events({
    'change input':function(event){
      vars = {};
      vars.capt = $('#teamCapt').val();
      vars.description = $('#teamDesc').val();
      vars.teamName = $('#teamName').val();
      var isTeamNameTaken = Teams.find({teamName: vars.teamName}).count()>0;
      if(isTeamNameTaken){
        vars.teamName = "";
      }
      console.log("Vars: " + JSON.stringify(vars));
      Meteor.call('updateSettings', Meteor.userId(), vars);
    }
  })

  Template.teamViewPage.helpers({
    currentTeam: function(){
      return Teams.findOne({userID: Meteor.userId()});
    },
    rating: function(){
      team = Teams.findOne({_id: currentViewingTeam});
      currentUser = Meteor.userId();
      ratings = team.ratings;
      currentTeamRating = {};
      ratings.forEach(function(rating, index, array){
        if(rating.raterId == currentUser){
          currentTeamRating = rating;
        }
      });

      return currentTeamRating;
    },
    isAdmin: function(){
      //console.log(Teams.findOne({userID: Meteor.userId()}).isAdmin);
      return Teams.findOne({userID: Meteor.userId()}).isAdmin;
    },
    isSaved: function(){
      savedTeams = Teams.findOne({userID: Meteor.userId()}).savedTeams;
      isSaved = false;
      //console.log("checking saves")
      for(i = 0; i <= savedTeams.length; i += 1){
        //console.log("saved Team "+i+": "+savedTeams[i]);
        //console.log("currentViewingTeam: " + currentViewingTeam);
        if(savedTeams[i] == currentViewingTeam){
          //console.log("Team is saved");
          isSaved = true;
          return true;
        }
      }
      if(!isSaved){
        return false;
      }

    }
  })
  Template.body.helpers({
    team: function(){
      return Teams.findOne({userID: Meteor.userId()});
    }
  })

  Template.settingsPage.helpers({
    team: function(){
      return Teams.findOne({userID: Meteor.userId()});
    }
  })

  //RATINGS PAGE

  Template.ratings.helpers({
    team: function(){
      savedTeams = Teams.findOne({userID: Meteor.userId()}).savedTeams;
      savedTeamArray;

      savedTeams.forEach(function(teamID, index, array){
        savedTeamArray.push(Teams.findOne({_id: teamID}));
      });

      return savedTeamArray;

    }
  })

  Template.ratings.onCreated(function(){
    this.subscribe('Teams');
  })

  Template.ratings.helpers({
    rating: function(){
      return Teams.findOne({userID: Meteor.userId()}).ratings;
    }
  })

  //END OF RATINGS PAGE


  //SAVED TEAM PAGE

  Template.savedTeamView.events({
    'click .list-group-item': function(event){
      currentViewingTeam = event.currentTarget.getAttribute('teamId');
    }
  })

  Template.savedTeamView.helpers({
    savedTeam: function(){
      savedTeamIds = Teams.findOne({userID: Meteor.userId()}).savedTeams;
      savedTeams = [];

      for( i = 0; i < savedTeamIds.length; i += 1 ){
        savedTeams.push(Teams.findOne({_id: savedTeamIds[i]}));
      }
      if(savedTeams.length === 0){
        savedTeams = null;
      }
      return savedTeams;

    },
    makeDragabble: function(){
      savedTeamIds = Teams.findOne({userID: Meteor.userId()}).savedTeams;

      for( i = 0; i < savedTeamIds.length; i += 1 ){

      }
      return "";
    }

  })
0
  //END OF SAVED TEAM PAGE

  //TOP BAR TEMPLATE

  Template.topBar.onCreated(function(){
    this.subscribe('Teams');
  })

  //END OF TOP BAR TEMPLATE


  // HOME PAGE

   Template.homePage.helpers({
    user: function(){
      return JSON.stringify(Meteor.user());
    },
    team: function(){
      return Teams.findOne({userID: Meteor.userId()});
    }
  })

  Template.homePage.events({
    "click #statsButton":function(event){
      currentViewingTeam = Teams.findOne({userID: Meteor.userId()})._id;
      //swal("good job!", "You clicked a button!", "success");
    }
  })

  Template.homePage.onRendered(function(){
    //console.log("Page rendered!");
  })

  //END OF HOME PAGE



  //TEAM LIST PAGE
  Template.teamListPage.events({
    'click .list-group-item': function(event){
      currentViewingTeam = event.currentTarget.getAttribute('teamId');
    },
    'click #gottaQuestion': function(event){
      //swal({   title: "HTML <small>Title</small>!",   text: "A custom <span style="color:#F8BB86">html<span> message.",   html: true });
      swal("Test", "Yup this is a test");
    }
  })

  Template.teamListPage.helpers({
    teams: function(){
      return Teams.find({});
    }
  });

  //END OF TEAM LIST PAGE
  Template.teamViewPage.onCreated(function(){
    this.subscribe('Teams');
  })
  Template.teamViewPage.events({
    'change #teamIsChosen':function(){
      Meteor.call('changeIsTeamTaken', currentViewingTeam, {isTaken: $("#teamIsChosen").is(':checked')});
      //console.log($("#teamIsChosen"))
      //console.log("Is taken: " + $("#teamIsChosen").is(':checked'));
    },
    'change input[type=range]': function(event){
      var viewedTeam = $("meta[name=teamID]").attr('value');
      var value = event.currentTarget.value;
      var teamRatings = Teams.findOne({_id: viewedTeam}).ratings;
      var displayBox = $("#"+event.currentTarget.getAttribute('valfor'));
      var ratingFor = event.currentTarget.getAttribute('valfor');
      var foundRating = false;
      var newRating = {};
      displayBox.html(value);
      //console.log("Ratings: " + teamRatings);
      teamRatings.forEach(function(rating, index, array){
        if(rating.raterId == Meteor.userId()){
          foundRating = true;
          if(ratingFor == "launcherRating"){
            rating.launcherSpeedRating = value;
          }else if(ratingFor == "intakeRating"){
            rating.intakeSpeedRating = value;
          }else if(ratingFor == "liftRating"){
            rating.liftRating = value;
          }else if(ratingFor == "launcherAccuracy"){
            rating.launcherAccuracy = value;
          }
        }
      });

      if(!foundRating){
        var rating = {
          raterId: Meteor.userId(),
          launcherSpeedRating: 0,
          intakeSpeedRating: 0,
          liftRating: 0,
          launcherAccuracy: 0
        }
        if(rating.raterId == Meteor.userId()){
          foundRating = true;
          if(ratingFor == "launcherRating"){
            rating.launcherSpeedRating = value;
          }else if(ratingFor == "intakeRating"){
            rating.intakeSpeedRating = value;
          }else if(ratingFor == "liftRating"){
            rating.liftRating = value;
          }else if(ratingFor == "launcherAccuracy"){
            rating.launcherAccuracy = value;
          }
        }
        teamRatings.push(rating);
      }
      //console.log("Ratings: " + JSON.stringify(teamRatings));
      Meteor.call('updateTeamRating', {teamId: viewedTeam, teamRating: teamRatings});
      Meteor.call('updateTeamOverall', viewedTeam, {});
    },
    'click #saveTeam': function(){
      Meteor.call('saveTeam', Meteor.userId(), {team: currentViewingTeam});
    },
    'click #unsaveTeam': function(){
      teamList = Teams.findOne({userID: Meteor.userId()}).savedTeams;
      teamList.splice(currentViewingTeam, 1);
      Meteor.call('updateSavedTeams', Meteor.userId(), {teamList: teamList});
    }
  })

  Template.register.events({
    'submit form':function(event){
      event.preventDefault();
      var teamName = $('[name=teamName]').val();
      var captainName = $('[name=captainName]').val();
      var password = $('[name=teamPassword]').val();
      var passwordTwo = $('[name=teamPasswordTwo]').val();
      var email = $('[name=teamEmail]').val();

      var isEmailTaken = Meteor.users.find({"emails.address": email}, {limit: 1}).count()>0
      var isTeamNameTaken = Teams.find({teamName: teamName}).count()>0;

      console.log(email);
      console.log("Email results: " + JSON.stringify(isEmailTaken) + "  " + Meteor.users.find({"emails.address": email}, {limit: 1}).count()>0);
      console.log("Team name results: " + JSON.stringify(isTeamNameTaken) + "  " + Teams.find({teamName: teamName}).count()>0);

      if(isTeamNameTaken){
        console.log("team name is taken")
      }else if(password.length < 8){
        $("#registerError").html("Password too short");
      }else if(password !== passwordTwo){
        $("#registerError").html("Passwords don't match");
      }else{
        var userID = Accounts.createUser({
        email: email,
        password: password,
        }, function(error){
          if(error){
            console.log(error.reason);
            $("#registerError").html(error.reason);
          }else{
            Meteor.call('addTeam', Meteor.userId(), {captainName: captainName, teamName: teamName});
            Router.go('home');
          }
        });
        //Accounts.sendVerificationEmail(userID);

      }

    }

  });

  Template.login.events({
    'submit form': function(event){
        event.preventDefault();
        var teamEmail = $('[name=teamEmail]').val();
        var teamPassword = $('[name=teamPassword]').val();
        Meteor.loginWithPassword(teamEmail, teamPassword, function(error){
          if(error){
            $("#loginError").html("Team name or password invalid");
          }else{
            //console.log(JSON.stringify(Meteor.user()));
            Session.set("currentTeam", Teams.findOne({userID: Meteor.userId()}));
            Router.go('home');
          }
        });
    }
  });

  Template.navbar.events({
    'click .logout': function(event){
        event.preventDefault();
        Router.go('/');
    }
  });


  Template.registerHelper('equals', //Universal helper (checking if things are equal in spacebars)
    function(v1, v2) {
        return (v1 === v2);
  })

  Template.registerHelper('adsDisabled', //Universal helper (Checks if ads are disabled for current user)
    function(v1, v2) {
        team = Teams.findOne({userID: Meteor.userId()});
        //console.log("DoBlockAds: " + team.adsDisabled);
        return !team.adsDisabled;
  })

  Template.adminPage.helpers({
    teams:function(){
      return Teams.find();
    }

  })

  Template.adminPage.events({
    'change .adSetting':function(event){0
      teamID = event.currentTarget.getAttribute('forTeam');
      value = $("#"+teamID).is(":checked");
      Meteor.call('changeAdDisable', teamID, {isDisabled: value})
    },
    'change .blockSetting':function(event){
      teamID = event.currentTarget.getAttribute('forTeam');
      value = $('#'+teamID+"1").is(":checked");
      //console.log("Team blocked for all users: " + teamID + ": " + value);
      Meteor.call('changeTeamBan', teamID, {isBlocked: value});
    },
    'click .sendNote': function(event){
      //{id: "1", type: "info", asap: true, Title: "Welcome!", desc: "Welcome to vexify we are cool!" }
      teamID = event.currentTarget.getAttribute('forTeam');
      Meteor.call('addNotification', teamID, {type: "info", asap: true, Title: "Test notification", desc: "This is a test notification from an admin!"});
    }

  })


  //End of user js
}
