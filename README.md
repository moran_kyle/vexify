# README #

### What is this repository for? ###

* This is a app that allows teams to observe and rate each other during a competition. Its just mostly designed to make my life easier as a captain of a team. The app allows you to scope out your competition or keep a close eye on possible alliances. Right now this app is designed for the 2015-2016 challenge called Nothing But Net. But in the future can be made modular for multiple years/challenges.  
* V0.3.4

### How do I get set up? ###

* Well setting things up for the user should be as easy as either visiting the site or installing the app
* Configuration is really only on the server side right now. 
* This meteor app requires you to have iron:router, accounts-password and i believe that is it currently.... 
* Database configuration should automatically be done but contact the repo owner if its broken. 
* There is no tests written for this app so you are going to have to cry about it....

### Contribution guidelines ###

* Writing tests: moran_kyle
* Code review: moran_kyle
* Other guidelines: moran_kyle

### Who do I talk to? ###

* kylemoranp@gmail.com
* Other community